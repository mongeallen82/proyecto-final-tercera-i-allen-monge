console.log('Cargando Accordion...');
const dataAccordion = [
    {
        "title": "Mi Blog Personal",
        "desc": "Gracias por ingresar a mi block, mi nombre es Allen.  El ciclismo de montaña y el futbol son mis pasatiempos, prefiero la comida tradicional a la comida rápida."
    },
    {
        "title": "Noticias Nuevas",
        "desc": "Nuevos tutoriales ya están disponibles en la sección Galería-  100 nuevos expositores se han unido al blog- Recuerda los canales de contacto mediante correo o redes sociales ."
    },
    {
        "title": "Opiniones",
        "desc": "Comparte sobre tus puntos de vista,  temas relacionados con la política o la religión estan prohibidos dentro del  blog, las experiencias de vida son la razón de ser de esta sección."
    },

];

(function () {
    let ACCORDION = {
        init: function () {
            let _self = this;
            //Llamamos las funciones
            this.insertData(_self);
            this.eventHandler(_self);
        },

        eventHandler: function (_self) {
            let arrayRefs = document.querySelectorAll('.accordion-title');

            for (let x = 0; x < arrayRefs.length; x++) {
                arrayRefs[x].addEventListener('click', function (event){
                    console.log('event', event);
                    _self.showTab(event.target);
                });
            }
        },

        showTab: function(refItem) {
            let activeTab = document.querySelector('.tab-active');

            if(activeTab){
                activeTab.classList.remove('tab-active');
            }

            console.log('show tab', refItem);
            refItem.parentElement.classList.toggle('tab-active');
        },

        insertData: function (_self) {
            dataAccordion.map(function (item, index) {
                document.querySelector('.main-accordion-container').insertAdjacentHTML('beforeend', _self.tplAccordionItem(item));
            });
        },

        tplAccordionItem: function (item) {
            return (`<div class='accordion-item'>
        <div class='accordion-title'><p>${item.title}</p></div>
        <div class='accordion-desc'><p>${item.desc}</p></div>
        </div>`)
        },




    }
    ACCORDION.init();
})();