console.log('Cargando Cards...');
const dataCards = [
    {
        "title": "Título Técnico Superior en Calidad y Productividad ",
        "url_image": "https://i.pinimg.com/originals/cc/b1/e3/ccb1e3846e1d1b9a56123abcbcd4a5cf.jpg",
        "desc": "Este título se me otorgó en el año 1995, cuando cursaba el  noveno año de secundaria ",
        "cta": "Mostrar más",
        "link": "https://www.master-malaga.com/empresas/criterios-nivel-calidad-producto/"

    },
    {
        "title": "Título Ingenieria en Sistemas ",
        "url_image": "https://sites.google.com/site/itcsiscomp/_/rsrc/1452969968211/home/Ing_Sistemas_Banner.jpg",
        "desc": "Este título se me otorgó en el año 1995, cuando cursaba el  noveno año de secundaria ",
        "cta": "Mostrar más",
        "link": "https://www.master-malaga.com/empresas/criterios-nivel-calidad-producto/"

    },

    {
        "title": "Título Técnico Superior en Calidad y Productividad ",
        "url_image": "https://i.pinimg.com/originals/cc/b1/e3/ccb1e3846e1d1b9a56123abcbcd4a5cf.jpg",
        "desc": "Este título se me otorgó en el año 1995, cuando cursaba el  noveno año de secundaria ",
        "cta": "Mostrar más",
        "link": "https://www.master-malaga.com/empresas/criterios-nivel-calidad-producto/"

    },

    {
        "title": "Título Técnico Superior en Calidad y Productividad ",
        "url_image": "https://i.pinimg.com/originals/cc/b1/e3/ccb1e3846e1d1b9a56123abcbcd4a5cf.jpg",
        "desc": "Este título se me otorgó en el año 1995, cuando cursaba el  noveno año de secundaria ",
        "cta": "Mostrar más",
        "link": "https://www.master-malaga.com/empresas/criterios-nivel-calidad-producto/"

    },
    {
        "title": "Título Técnico Superior en Calidad y Productividad ",
        "url_image": "https://i.pinimg.com/originals/cc/b1/e3/ccb1e3846e1d1b9a56123abcbcd4a5cf.jpg",
        "desc": "Este título se me otorgó en el año 1995, cuando cursaba el  noveno año de secundaria ",
        "cta": "Mostrar más",
        "link": "https://www.master-malaga.com/empresas/criterios-nivel-calidad-producto/"

    },
    {
        "title": "Título Técnico Superior en Calidad y Productividad ",
        "url_image": "https://i.pinimg.com/originals/cc/b1/e3/ccb1e3846e1d1b9a56123abcbcd4a5cf.jpg",
        "desc": "Este título se me otorgó en el año 1995, cuando cursaba el  noveno año de secundaria ",
        "cta": "Mostrar más",
        "link": "https://www.master-malaga.com/empresas/criterios-nivel-calidad-producto/"
    }];


(function () {
    let CARD = {
        init: function () {
            console.log('El modulo de arga funciona correctamente');
            let _self = this;
            //Llamamos a las funciones
            this.insertData(_self);
        },

        eventHandler: function (_self) {
            let arrayRefs = document.querySelectorAll('.accordion-title');

            for (let x = 0; x < arrayRefs.length; x++) {
                arrayRefs[x].addEventListener('click', function (event) {
                    console.log('evento: ', event);
                    _self.showTab(event.target);
                });
            }
        },

        insertData: function (_self) {
            dataCards.map(function (item, index) {
                document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));

            });
        },
      
        tplCardItem: function (item, index){
            return(`<div class='card-item' id="card-number-${index}">
            <img src="${item.url_image}"/>
            <div class="card-info">
            <p class='card-title'>${item.title}</p>
            <p class='card-desc'>${item.desc}</p>
            <a class='card-cta' target='blank' href="${item.link}">${item.cta}</a>
            </div>
            </div>`)
        },
    }

    CARD.init();
})();